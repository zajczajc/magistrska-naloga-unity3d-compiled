# Magistrska naloga Unity3D



# Keyboard inputs
W, A, S, D - camera movement
Q, E - camera rotation
Y, X - Zoom in and out
Esc - Quit to Main Menu

# Mouse inputs
Right mouse button + mouse movement - camera rotation
Mouse scroll - Zoom in and out
